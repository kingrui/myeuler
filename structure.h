#ifndef _HALF_EDGE_
#define _HALF_EDGE_

class Solid;
class Face;
class Loop;
class Edge;
class HalfEdge;
class Vertex;
class Point;

class Solid{
private:
    int id;
    Solid *prev;
    Solid *next;
    
    Face *face;
    Edge *edge;
    Vertex *vertex;
public:
    Solid():prev(nullptr),next(nullptr),face(nullptr),edge(nullptr),vertex(nullptr){}
    ~Solid(){}
    
    getId(){return id;}
    getPrev(){return prev;}
    getNext(){return next;}
    
    getFace(){return face;}
    getEdge(){return edge;}
    getVertex(){return vertex;}


    
};
class Face{
private:
    int id;
    Solid *solid;
    
    Loop *outerLoop;
    Loop *innerLoop;
    
public:
    Face(Solid *ts):solid(ts),outerLoop(nullptr),innerLoop(nullptr){}
    ~Face(){}
    
    getId(){return id;}
    getSolid(){return solid;}
    
    
};
class Loop{
private:
    Face *face;
    
    HalfEdge *halfEdge;
    
    Loop *prev;
    LOOP *next;
    
public:
    Loop(Face *vf):face(vf),halfEdge(nullptr),prev(nullptr),next(nullptr){}
    ~Loop(){}
    
    getFace(){return face;}
    getHalfEdge(){return halfEdge;}
    getPrev(){return prev;}
    getNext(){return next;}
};
class Edge{
private:
    HalfEdge *leftHalf;
    HalfEdge *rightHalf;
    
    Edge *prev;
    Edge *next;
    
public:
    Edge(HalfEdge *left,HalfEdge *right):leftHalf(left),rightHalf(right),prev(nullptr),next(nullptr){}
    ~Edge(){}
    
    getLeftHalf(){return leftHalf;}
    getRightHalf(){return rightHalf;}
    getPrev(){return prev;}
    getNext(){return next;}
};
class HalfEdge{
public:
    Vertex *vertex;
    
    Loop *loop;
    Edge *edge;
    
    HalfEdge *prev;
    HalfEdge *next;
    HalfEdge *otherHalf;
};
class Vertex{
public:
    int id;
    Point point;
    
    Vertex *prev;
    Vertex *next;
};

class Point{
private:
    double x;
    double y;
    double z;
    
public:
    Point(double tx,double ty,double tz):x(tx),y(ty),z(tx){}
    ~Point(){}
    double getX(){return x;}
    double getY(){return y;}
    double getZ(){return z;}
};

#endif